import logo from './logo.svg';
import './App.css';
import Home from './components/Home';
import { Route, BrowserRouter, } from "react-router-dom"
import AddProduct from './components/AddProduct';
import New from './new';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Route exact path="/" component={Home} />

      </div>
    </BrowserRouter>
  );
}

export default App;
