import React, { Component } from 'react'
import Header from './Header'
import Products from './Products'

export class Home extends Component {
    render() {
        return (
            <div>
                <Header />
                <Products />
            </div>

        )
    }
}

export default Home