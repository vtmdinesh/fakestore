import React from 'react'

function ProductTile(props) {
    let { id, title, price, description, image } = props.productData
    let getIdOfSelected = props.function
    // console.log(id)
    // console.log(props)

    let getId = () => {
        getIdOfSelected(id)
    }

    return (
        <div className='col-12'>
            <div className="card mb-3" onClick={getId}>
                <div className="row g-0">
                    <div className="col-md-4 h-25">
                        <img src={image} className="rounded-start h-25 w-50" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title">{title}</h5>
                            <p className="card-text"></p>
                            <p className="card-text"><small className="text-muted">Price: {price}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ProductTile