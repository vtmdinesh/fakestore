import React from "react"
// import "./errorPage.css"
const ErrorPage = () => {



    return (
        <div className="d-flex flex-column flex-md-row align-items-center">
            <div className="text-center w-50">
                <h3 className="fs-1" >OOPS !!!  </h3>
                <h5 className="fs-3"> Page Not Found</h5>
                <p className="fs-3">This page was removed or never existed</p>

            </div>

            <img src="./errorPage.jpeg" className="w-50 h-100" alt="error" />

        </div>

    )
}

export default ErrorPage