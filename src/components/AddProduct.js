import React from 'react'
import validator from 'validator'

function AddProduct(props) {

    let [addProduct, updateNewData, updateRating, updateForm, resetForm] = props.function
    let isFormSubmitted = props.formStatus
    console.log(isFormSubmitted)
    let { title, description, price, rating, image, category } = props.productData
    // console.log(rating)



    let resetFormFunction = () => {

        let newProductData = {
            description: "",
            title: "",
            image: "",
            category: "",
            price: "",
            rating: { rate: "", count: "" },
        }
        resetForm(newProductData)



    }

    let showForm = () => {
        updateForm(false)
    }


    let updateFormStatus = (value) => {
        console.log("im called addproduct")
        updateForm(value)
    }

    let addProductData = () => {

        console.log("in");
        let isTitleValid = (title) ? ((!validator.isEmpty(title) ? true : false)) : false
        let isDescriptionValid = (description) ? ((!validator.isEmpty(description) ? true : false)) : false
        let isCategoryValid = (category) ? ((!validator.isEmpty(category)) ? true : false) : false
        let isPriceValid = (price >= 0)
        let isCountValid = (rating.count >= 0)
        let isRateValid = (rating.rate < 5 && rating.rate > 0)
        let isimageValid = validator.isURL(image)

        if (isTitleValid && isDescriptionValid && isCategoryValid && isPriceValid && isRateValid && isimageValid && isCountValid) {
            console.log("all validated");
            addProduct(props.productData)
            updateFormStatus(true)
            resetFormFunction();

        }


    }

    let getInput = (event) => {
        let content = event.target.value
        let name = event.target.name
        updateNewData(name, content)



    }

    let getPriceInput = (event) => {
        let price = event.target.value
        updateNewData("price", price)
    }

    let getRatingInput = (event) => {
        let value = event.target.value
        let name = event.target.name


        updateRating(name, value)




    }

    let form = <div className='w-100 vh-75 container border border-secondary rounded-3 p-5'>
        <h1 className='fs-5'>ADD PRODUCT DETAILS</h1>
        <div className='d-flex flex-column align-items-start'>
            <div className="input-group mb-3 ">
                <span className="input-group-text" id="basic-addon1" >Title</span>
                <input type="text" onChange={getInput} name="title" id="titleInput" className="form-control" placeholder="Edit title" aria-label="Title" aria-describedby="basic-addon1" />
            </div>
            {(title) ? <p></p> : <p style={{ "color": "red" }}>Title cannot be empty</p>}


        </div>
        <div>
            {(true) ? <p></p> : <p style={{ "color": "red" }}>Title cannot be empty</p>}
        </div>
        <div className='d-flex flex-column align-items-start'>  <div className="input-group mb-3">
            <span className="input-group-text">Description</span>
            <textarea rows="5" onChange={getInput} id="descriptionInput" name="description" className="form-control" aria-label="Description" placeholder='Enter product description'></textarea>
        </div> {(description) ? <p></p> : <p style={{ "color": "red" }}>Description cannot be empty</p>}
        </div>
        <div className='d-flex flex-column align-items-start'>
            <div className="input-group price-container w-50 mb-3">
                <span className="input-group-text">Price $</span>
                <input type="number" onChange={getPriceInput} id="priceInput" className="form-control" value={props.productData.price} aria-label="Edit price" placeholder='Price' />
            </div>

            {(price <= 0 || "") ? <p style={{ "color": "red" }}> Price should be greater than zero. </p> : <p></p>}
        </div>
        <div className='d-flex flex-column align-items-start'>
            <div className="input-group   w-100 mb-3" >
                <span className="input-group-text" id="basic-addon3">Image URL</span>
                <input type="text" name="image" id="imageInput" className="form-control" onChange={getInput} aria-describedby="basic-addon3" placeholder='Enter image url' />
            </div>

            {(image) ? <p></p> : <p style={{ "color": "red" }}>Plesase enter valid image url </p>}

        </div>
        <div className="input-group">
            <div className='w-100 '>
                <span className="input-group-text">Rating</span>
                <input type="number" aria-label="Rate" value={props.productData.rating.rate} placeholder="Edit rate" name="rate" onChange={getRatingInput} id="rate" className="form-control" />
                {
                    (rating.rate < 5 && rating.rate > 0) ? <p></p> : <p style={{ "color": "red" }}>Rate should be 0-5</p>
                }
            </div>
            <div className='w-100'>
                <span className="input-group-text">Count</span>
                <input type="number" aria-label="Count" value={props.productData.rating.count} placeholder="Edit count" onChange={getRatingInput} id="Count" name="count" className="form-control" />
                {
                    (rating.count > 0) ? <p></p> : <p style={{ "color": "red" }}>Count should not be less than zero..</p>
                }
            </div>

        </div>
        <div className='d-flex flex-column align-items-start'>
            <div className="input-group mb-3 ">
                <span className="input-group-text" id="basic-addon1" >Category</span>
                <input type="text" onChange={getInput} name="category" id="categoryInput" className="form-control" placeholder="Edit category" aria-label="Category" aria-describedby="basic-addon1" />
            </div>
            {(category) ? <p></p> : <p style={{ "color": "red" }}>Category cannot be empty</p>}


        </div>


        <button className='btn btn-primary' onClick={addProductData}>Add Product</button>

    </div>

    let successData = <div className=''>
        <h1 className='text-success fs-4'>Your Product added successFully</h1>
        <button className='btn btn-primary' onClick={showForm}>Add more products</button>
    </div>


    return (


        <div>

            {isFormSubmitted ? successData : form}
        </div>





    )
}

export default AddProduct