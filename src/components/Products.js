import React, { Component } from 'react'
import ProductTile from './ProductTile'
import IndividualProduct from './IndividualProduct'
import Spinner from "./Spinner"
import ErrorPage from './ErrorPage'
import "../App.css"
import AddProduct from './AddProduct'
import DeleteProduct from './DeleteProduct'
import axios from 'axios'


export class Products extends Component {
    constructor(props) {
        super(props)
        this.APP_STATE = {
            COMPLETED: "COMPLETED",
            IN_PROGRESS: "IN_PROGRESS",
            FAILED: "FAILED"
        }
        this.CRUD_MODE = {
            EDIT: "EDIT",
            ADD: "ADD",
            DELETE: "DELETE"
        }

        this.state = {
            productsList: [],
            selectedId: 1,
            currentMode: "EDIT",
            formStatus: false,
            isDeleted: false,
            newProductData: {
                description: "",
                title: "",
                image: "",
                price: "",
                category: "",
                rating: { rate: "", count: "" }
            },


        }
    }

    componentDidMount() {
        this.getData()

    }

    getIdOfSelected = (id) => {
        this.setState({ selectedId: id })
    }

    updateNewProduct = (name, value) => {
        let obj = this.state.newProductData
        obj[name] = value
        let stateObj = {}
        this.setState((prevState) => {
            stateObj["newProductData"] = prevState.newProductData
            return stateObj
        })
    }

    updateForm = (status) => {
        console.log("im called product")
        this.setState({ formStatus: status })
    }


    updateData = (id, name, value) => {
        const selectedObject = this.state.productsList.filter((item) => item.id === id).pop();
        selectedObject[name] = value;
        let stateObj = {}
        this.setState((prevState) => {
            stateObj["productsList"] = prevState.productsList
            return stateObj
        })
    }
    updateNewRating = (name, value) => {
        let obj = this.state.newProductData

        if (obj.rating === undefined) {
            obj.rating = {}
        }
        obj.rating[name] = value
        let stateObj = {}
        this.setState((prevState) => {
            stateObj["newProductData"] = prevState.newProductData
            return stateObj
        })
    }
    updateRating = (id, name, value) => {
        const selectedObj = this.state.productsList.filter((eachProduct) => eachProduct.id === id).pop()
        selectedObj.rating[name] = value

        let stateObj = {}
        this.setState((prevState) => {
            stateObj["productsList"] = prevState.productsList
            return stateObj
        })
    }


    updatePrice = (id, price) => {
        let selectedObject = this.state.productsList.filter((item) => item.id === id).pop()
        selectedObject.price = price;
        let stateObj = {}
        this.setState((prevState) => {
            stateObj["productsList"] = prevState.productsList
            return stateObj
        })
    }

    resetForm = (obj) => {
        this.setState({
            newProductData: obj
        })
    }

    updateProduct = (updatedData, id) => {

        axios({
            method: 'put',
            url: `https://fakestoreapi.com/products/${id}`,
            data: JSON.stringify(updatedData)
        }).then(response => {
            console.table(response.data)
        }).catch(err => {
            console.error(err)
            this.setState({ currentState: this.APP_STATE.FAILED })
        })
    }

    addProduct = (productData) => {
        axios({
            method: 'post',
            url: `https://fakestoreapi.com/products/`,
            data: JSON.stringify(productData)
        })
            .then(response => response.data)
            .then((data) => {
                this.setState({
                    productsList: [...this.state.productsList, data]
                })
            })
            .catch(err => {
                console.error(err)
                this.setState({ currentState: this.APP_STATE.FAILED })
            })


    }

    deleteProduct = (id) => {
        axios.delete(`https://fakestoreapi.com/products/${id}`)
            .then((res) => res.data)
            .then((data) => {
                const updatedData = this.state.productsList.filter((eachProduct) => eachProduct.id !== data.id)

                this.setState({
                    productsList: updatedData,
                    isDeleted: true
                })

            })
            .catch(err => {
                console.error(err)
            })
    }

    resetDelete = () => {
        this.setState({ isDeleted: false })
        console.log("in products");
    }




    getData = () => {

        let url = "https://fakestoreapi.com/products"

        this.setState({ currentState: this.APP_STATE.IN_PROGRESS })
        axios
            .get(url).then(response => {
                this.setState({
                    productsList: response.data,
                    currentState: this.APP_STATE.COMPLETED
                })
            }

            ).catch(err => {
                console.error(err)
                this.setState({ currentState: this.APP_STATE.FAILED })
            })
    }
    onChangeMode = (event) => {
        let mode = event.target.value
        this.setState({ currentMode: mode })
        this.resetDelete()
    }

    render() {
        let productsList = this.state.productsList
        // console.log(productsList)
        let no_of_items = (Object.keys(productsList)).length
        let isAdd = (this.state.currentMode === this.CRUD_MODE.Add) ? "activeBtn" : null
        let isEdit = (this.state.currentMode === this.CRUD_MODE.EDIT) ? "activeBtn" : null
        let isDelete = (this.state.currentMode === this.CRUD_MODE.DELETE) ? "activeBtn" : null


        let selectedId = this.state.selectedId
        return (

            <div className='container-fluid' >
                <div className='row'>
                    {(this.state.currentState === this.APP_STATE.IN_PROGRESS) ? <Spinner /> : null}
                    {(this.state.currentState === this.APP_STATE.FAILED) ? <ErrorPage /> : null}
                    <div className=' col-md-5 col-sm-12 products-container overflow-auto'>
                        {(this.state.currentState === this.APP_STATE.COMPLETED) ? ((no_of_items) ? (productsList.map(eachProduct => {
                            return <ProductTile productData={eachProduct} function={this.getIdOfSelected} key={eachProduct.id}
                            />
                        })) : null
                        ) : null}

                    </div>

                    <div className='col-sm-12 col-md-7 '>
                        <div className='mt-3 d-flex flex-row justify-content-center'>
                            <button className={`btn crud ${isAdd} btn-primary m-1`} value="ADD" onClick={this.onChangeMode} >ADD</button>

                            <button className={` btn crud  ${isEdit} btn-primary m-1`} value="EDIT" onClick={this.onChangeMode}>EDIT</button>

                            <button className={`btn crud   ${isDelete} btn-primary m-1`} value="DELETE" onClick={this.onChangeMode} >DELETE</button>

                        </div>
                        {((this.state.currentState === this.APP_STATE.COMPLETED) && (this.state.currentMode === this.CRUD_MODE.ADD)) ? (<div className='mt-2'>
                            <AddProduct productData={this.state.newProductData} formStatus={this.state.formStatus} function={[this.addProduct, this.updateNewProduct, this.updateNewRating, this.updateForm, this.resetForm]} />
                        </div>) : null}
                        {((this.state.currentState === this.APP_STATE.COMPLETED) && (this.state.currentMode === this.CRUD_MODE.EDIT)) ? (<div className='mt-2'>
                            <IndividualProduct data={productsList[selectedId - 1]} mode={this.state.CRUD_MODE} function={[this.updateData, this.updatePrice, this.updateProduct, this.updateRating]} key={selectedId} />
                        </div>) : null}

                        {((this.state.currentState === this.APP_STATE.COMPLETED) && (this.state.currentMode === this.CRUD_MODE.DELETE)) ? (<div className='mt-2'>
                            <DeleteProduct function={[this.deleteProduct, this.resetDelete]} data={[this.state.isDeleted, selectedId]} />
                        </div>) : null}


                    </div>
                </div>
            </div >


        )
    }
}

export default Products