import React from 'react'
import { Link } from "react-router-dom"
import "../App.css"

function Header() {
    return (
        <nav className="navbar navbar-expand-md navbar-dark sticky-top bg-primary">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/"><i className="fa-solid fa-house icon"></i></Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                        <Link className="nav-link" to="/features">Features</Link>
                        <Link className="nav-link" to="/services">Services</Link>
                        <Link className="nav-link" to="/contact">Contact</Link>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Header