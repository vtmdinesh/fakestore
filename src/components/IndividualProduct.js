import React from 'react'
import "../App.css"

function IndividualProduct(props) {
    let { id, title, description, price, rating, image, category } = props.data

    let [updateDataFunction, updatePriceFunction, updateProduct, updateRating] = props.function
    // console.log(props.data)

    let updateProductData = () => {
        let updatedData = {
            id, title, description, price, rating, image, category

        }
        updateProduct(updatedData, id)

    }

    let getRatingInput = (event) => {
        let value = event.target.value
        let name = event.target.name


        if (name === "rate") {
            if (value > 5) {
                value = 5
                updateRating(id, name, 5)
            }
            else if (value < 0) {
                updateRating(id, name, 0)
            }
            else {
                updateRating(id, name, value)
            }

        }
        else if (value < 0) {
            updateRating(id, name, 0)
        }
        else {
            updateRating(id, name, value)
        }



    }

    let getInput = (event) => {
        let content = event.target.value
        let name = event.target.name
        updateDataFunction(id, name, content)

        // if (!content) {
        //     if (name === "title") {
        //         updateDataFunction(id, name, "Product Title")
        //     }
        //     else if (name === "description") {
        //         updateDataFunction(id, name, "Product Description")
        //     }

        // }


    }




    let getPriceInput = (event) => {
        let price = event.target.value
        if (price === "" || price < 0) {
            price = 0
            updatePriceFunction(id, 0)

        }
        else {
            updatePriceFunction(id, price)
        }

    }

    return (
        <div className=" w-100 vh-75 container border border-secondary rounded-3 d-flex flex-column align-items-center  pb-3 mb-2">
            <div className='row'>
                <div className='col-12'>
                    <div className='w-100 d-flex justify-content-center mb-2'>
                        <img src={image} alt="product" className="image" />
                    </div>

                    <div className="d-flex btn-container w-100 flex-column flex-md-row justify-content-center justify-content-md-around align-self-center  align-items-center mb-3 col-5">
                        <button className="btn btn-warning add-cart fs-6"><span> <i className="fa-solid fa-cart-arrow-down icon"></i></span>ADD TO CART</button>
                        <button className="btn btn-danger buy-now fs-6"> <span><i className="fa-solid fa-bolt-lightning icon"></i></span>BUY NOW</button>
                    </div>

                    <h3 className="mb-3 fs-3">
                        {title}</h3>
                    <h3 className="mb-3 fs-6 btn-secondary btn align-self-end text-start">
                        {category}</h3>
                    <div className='d-flex justify-content-between flex-md-row flex-column '>
                        {
                            //     <div className="rating-details-container d-flex align-items-center g-5 mb-2 ">
                            //     <p className="mr-3  btn  btn-success rating">{rating.rate}<span className='p-1'><i className="fa-solid fa-star-half-stroke"></i></span></p>
                            //     <p className='rating text-secondary'>{rating.count} Ratings</p>
                            // </div>
                        }

                        <p className="fs-5"> Price: ${price}</p>
                    </div>
                    <p className='text-start fs-6'>{description}</p>


                </div>
            </div>
            <div className='row '>
                <div className='col-12 w-100 vh-75 border border-secondary rounded-3 p-2'>

                    <h1 className='fs-5'>EDIT PRODUCT DETAILS</h1>
                    <div className='d-flex flex-column align-items-start'>                    <div className="input-group mb-3 ">
                        <span className="input-group-text" id="basic-addon1" >Title</span>
                        <input type="text" onChange={getInput} name="title" id="titleInput" value={title} className="form-control" placeholder="Edit title" aria-label="Title" aria-describedby="basic-addon1" />
                    </div>
                        {(title) ? <p></p> : <p style={{ "color": "red" }}>Title cannot be empty</p>}
                    </div>
                    <div className='d-flex flex-column align-items-start'>  <div className="input-group mb-3">
                        <span className="input-group-text">Description</span>
                        <textarea rows="5" value={description} onChange={getInput} id="descriptionInput" name="description" className="form-control" aria-label="Description" placeholder='Enter product description'></textarea>
                    </div>
                        {(description) ? <p></p> : <p style={{ "color": "red" }}>Description cannot be empty</p>}
                    </div>
                    <div className='d-flex flex-column align-items-start'>
                        <div className="input-group price-container w-50 mb-3">
                            <span className="input-group-text">Price $</span>
                            <input type="number" value={price} onChange={getPriceInput} id="priceInput" className="form-control" aria-label="Edit price" placeholder='Price' />
                        </div>
                        {(price <= 0 || "") ? <p style={{ "color": "red" }}> Price should be greater than zero. </p> : <p></p>}
                    </div>
                    <div className='d-flex flex-column align-items-start'>
                        <div className="input-group   w-100 mb-3" >
                            <span className="input-group-text" id="basic-addon3">Image URL</span>
                            <input type="text" value={image} name="image" id="imageInput" className="form-control" onChange={getInput} aria-describedby="basic-addon3" placeholder='Enter image url' />
                        </div>
                        {(image) ? <p></p> : <p style={{ "color": "red" }}>Image URL cannot be empty</p>}
                    </div>
                    {
                        //     <div className="input-group">
                        //     <span className="input-group-text">Rating and users</span>
                        //     <input type="number" aria-label="Rate" name="rate" placeholder="Edit rate" value={rating.rate} onChange={getRatingInput} id="rate" className="form-control" />
                        //     <input type="number" aria-label="Count" name="count" value={rating.count} placeholder="Edit count" onChange={getRatingInput} id="count" className="form-control" />
                        // </div>
                    }

                    <div className='d-flex flex-column align-items-start'>                    <div className="input-group mb-3 ">
                        <span className="input-group-text" id="basic-addon1" >Category</span>
                        <input type="text" onChange={getInput} name="category" id="categoryInput" value={category} className="form-control" placeholder="Edit category" aria-label="Category" aria-describedby="basic-addon1" />
                    </div>
                        {(category) ? <p></p> : <p style={{ "color": "red" }}>Category cannot be empty</p>}
                    </div>


                    <button className='btn btn-primary' onClick={updateProductData}>Update Product</button>
                </div>
            </div>
        </div>
    )
}

export default IndividualProduct