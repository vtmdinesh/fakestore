import React from 'react'

function DeleteProduct(props) {
    let [isdeleted, selectedId] = props.data
    let [deleteFunction] = props.function
    let userEnteredId
    let errMsg

    console.log(isdeleted)



    let deleteProduct = () => {
        deleteFunction(selectedId)
    }
    let getId = (event) => {
        console.log("onChange")
        userEnteredId = event.target.value
        if (userEnteredId > 20 || userEnteredId <= 0) {
            errMsg = "Id should be 1-20"
        }
        else {
            errMsg = ""
        }
    }
    let deleteUserEnteredId = () => {
        if (errMsg === "") {
            deleteFunction(userEnteredId)
        }
    }

    let successData = <div className=''>
        <h1 className='text-success fs-4'>Your Product Deleted successFully</h1>
        {
            // <button className='btn btn-primary' onClickCapture={resetFunction}>Delete More Products</button>
        }
    </div>
    let deletePage = <div className='d-flex flex-column justify-content-center align-items-start w-100 vh-75 container '>
        <h1 className='fs-2 align-self-center'>DELETE PRODUCT</h1>
        <button className='btn btn-primary del-btn mt-3' value={selectedId} onClick={deleteProduct}> Delete the selected Product</button>
        <h3 className='mt-3 ms-5'>-OR-</h3>

        <label htmlFor='idInput' className='fs-5 m-2'>Enter the id you want to delete </label>
        <div className='d-flex flex-column flex-md-row align-items-center'>
            <input type="number" className='p-1 w-75' id="idInput" onChange={getId} placeholder="Enter the id"></input>
            <p style={{ "color": "red" }}>{errMsg}</p>
            <button className='btn btn-primary m-2' onClick={deleteUserEnteredId}> Delete </button>
        </div>
    </div>

    return (
        <div className='d-flex flex-column justify-content-center align-items-start w-100 vh-75 container border border-secondary rounded-3'>
            {(isdeleted) ? successData : deletePage}
        </div>
    )
}

export default DeleteProduct